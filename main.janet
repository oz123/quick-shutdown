(use joy)
(import sh)

(defn app-layout [{:body body :request request}]
  (text/html
    (doctype :html5)
    [:html {:lang "en"}
     [:head
      [:title "quick-shutdown"]
      [:meta {:charset "utf-8"}]
      [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
      [:meta {:name "csrf-token" :content (csrf-token-value request)}]
      [:link {:href "/app.css" :rel "stylesheet"}]
      [:script {:src "/app.js" :defer ""}]]
     [:body
       body]]))


(defn home [request]
  [:div {:class "tc"}
   [:h1 "You found joy!"]
   [:p {:class "code"}
    [:b "Joy Version:"]
    [:span (string " " version)]]
   [:p {:class "code"}
    [:b "Janet Version:"]
    [:span janet/version]]])


(defn doer [request]
  (sh/$ ls -l)
  [:div {:classs "tc"} [:h1 "Farewell!"]]

  )

(defn blu [request]
 (application/json {:you-found "bar"})
)


(def routes (routes [:get "/" home]
                    [:get "/do" doer]
		    [:get "/bla" blu]))


(def app (-> (handler routes)
	     (layout app-layout)
             (with-csrf-token)
             (with-session)
             (extra-methods)
             (query-string)
             (body-parser)
             (json-body-parser)
             (server-error)
             (x-headers)
             (static-files)
             (not-found)
             (logger)))


(defn main [& args]
  (server app (env :port)))
