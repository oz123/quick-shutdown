(import tester :prefix "" :exit true)

(deftest
  (test "1 + 1 = 2"
    (is (= 2 (+ 1 1))))

  (test "1 + 3 = 4"
    (is (= 4 (+ 1 3))))

  (test "expected = actual"
    (let [expected "expected"
          actual "expected"]
      (is (= expected actual))))

  (test "errors can be tested"
    (is (= "hello" (catch (error "hello"))))))

