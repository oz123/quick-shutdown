(import testament :prefix "" :exit true)

(deftest one-plus-one
  (is (= 2 (+ 1 1)) "1 + 1 = 2"))

(deftest two-plus-two
  (is (= 5 (+ 2 2)) "2 + 2 = 5"))

(defn reporter
  "pretty report"
  [result]
  (print "Test: " (result :test))
  )


(set-on-result-hook reporter)
(run-tests!)
