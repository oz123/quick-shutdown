(declare-project
  :name "janet-demo"
  :description ""
  :dependencies ["https://github.com/joy-framework/joy"
                 "https://github.com/joy-framework/tester"
                 "https://github.com/pyrmont/testament"]
  :author ""
  :license ""
  :url ""
  :repo "")

(declare-executable
  :name "quick-shutdown"
  :entry "main.janet")

(phony "server" []
  (os/shell "janet main.janet"))

(phony "watch" []
  (os/shell "find . -name '*.janet' | entr -r -d janet main.janet"))

(phony "rtest" []
  (os/shell "find . -name '*.janet' | entr -r -d jpm test"))
